# GetrakChallenge

## Synopsis

Esta programa foi escrito em resposta ao Teste Técnico Getrak 2018.

Sua implementação prioriza simplicidade e manutenibilidade e considera uma arquitetura de fácil expansão.

Para testá-la, basta executar a aplicação e realizar uma requisição para http://localhost:8080/getstelarresult, informando o parâmetro
"distance" com a distância em MGLT.

### Trade offs

O design da solução visou tornar claro as seções de dados( quando e onde ) e dar coesão a cada método. 
Não foram aplicadas técnicas avançadas de tratamento de erro, testes unitários ou tampouco validações mais rebuscadas. 

Embora construída de maneira concisa, adicionar features como o agrupamento por classe de aeronave ou a ordenação por ordem 
alfabética podem sem realizadas com determinada segurança à solução.

## Installation

Clone o repositório:

git clone https://bitbucket.org/wcdiasdeoliveira/getrakchallenge/

Dentro do respectivo diretório, execute:

./gradlew build && java -jar build/libs/Kchallenge-0.0.1-SNAPSHOT.jar OR ./gradlew build run

## Contributors

Wesley C. Dias de Oliveira

## License

Free for use and extend.

