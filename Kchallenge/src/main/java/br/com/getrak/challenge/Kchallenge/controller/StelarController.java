package br.com.getrak.challenge.Kchallenge.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.getrak.challenge.Kchallenge.model.Starship;
import br.com.getrak.challenge.Kchallenge.model.StelarResult;

@RestController
public class StelarController{
	
	private RestTemplate restTemplate;
	
	private ArrayList<String> prepareResult(List<Starship> starships, Long distanceInMglt){
		ArrayList<String> result = new ArrayList<String>();
		for (Starship starship : starships) {
			StelarResult resultToAdd = new StelarResult();
			try {				
				int complexValue = starship.getMgltNormalizedAsInt() + 
								   starship.getCrewNormalizedAsInt() +
								   starship.getPassengersNormalizedAsInt() +
								   starship.getHyperdriveRatingNormalizedAsInt();
				 complexValue *= 100000;
				 resultToAdd.setDistanceInMglt(complexValue / distanceInMglt.intValue());
			}catch (Exception e) {
				resultToAdd.setDistanceInMglt(0);
			}
			resultToAdd.setName(starship.getName());
			result.add(resultToAdd.toString());
		}
		return result;
	}
	
	private HttpHeaders prepareHeaders() {
		 HttpHeaders headers = new HttpHeaders();
		 headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		 headers.add("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0");
	     headers.setContentType(MediaType.APPLICATION_JSON);
		 return headers;
	}
	
	private JSONObject executeRequest(int page) throws JSONException {
		String uri = "https://swapi.co/api/starships/";
		if(page != 0) {
			uri += "?page="+ page;
		}
		HttpEntity<String> response = this.getRestTemplate().exchange(uri, HttpMethod.GET, new HttpEntity<>(this.prepareHeaders()), String.class);
		JSONObject jsonObject = new JSONObject(response.getBody());
		return jsonObject;
	}
	
	private List<Starship> getStarships(){
		this.setRestTemplate(new RestTemplate());
		List<Starship> result = new ArrayList<Starship>();
		ObjectMapper mapper = new ObjectMapper();
		 boolean keepSearchingPages = true;
		 try {
			 for (int pageIndex = 0; keepSearchingPages; pageIndex++) {
				 JSONObject tempJsonObject = this.executeRequest(pageIndex);
				 result.addAll(mapper.readValue(tempJsonObject.get("results").toString(), new TypeReference<List<Starship>>(){}));
				 keepSearchingPages = !tempJsonObject.isNull("next");
				 System.out.println("starships => " + result.size());
			 }
		 } catch (Exception e) {
		  e.printStackTrace();
		 }
		return result;
	}
			
	@RequestMapping("/getstelarresult")
	public HttpEntity<JSONArray> getStelarResultByDistance(@RequestParam(value = "distance", required = true) Long distanceInMglt){
		List<String> result = this.prepareResult(this.getStarships(), distanceInMglt).stream()
			     .distinct()
			     .collect(Collectors.toList());
		 System.out.println("starships final result => " + result.size());
		return new ResponseEntity<>(new JSONArray(result), HttpStatus.OK);
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
}