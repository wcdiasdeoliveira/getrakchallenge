package br.com.getrak.challenge.Kchallenge.model;

public class StelarResult {
	
	private String name;
	private Integer distanceInMglt;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getDistanceInMglt() {
		return distanceInMglt;
	}
	
	public void setDistanceInMglt(Integer distanceInMglt) {
		this.distanceInMglt = distanceInMglt;
	}
	
	public String toString() {
		try {
			return String.format("%s: %d", this.getName(), this.getDistanceInMglt().intValue());	
		}catch (Exception e) {
			return String.format("%s: %d", this.getName(), 0);
		}
	}
}