package br.com.getrak.challenge.Kchallenge.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages= {"br.com.getrak.challenge.Kchallenge"})
public class KchallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(KchallengeApplication.class, args);
	}
}