package br.com.getrak.challenge.Kchallenge.model;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Starship extends ResourceSupport implements Serializable{
	
	private final static long serialVersionUID = 1l;
	
	private String name;
	private String model;
	private String manufacturer;
	private String cost;
	private String length;
	private String maxAtmospheringSpeed;
	private String crew;
	private String passengers;
	private String cargoCapacity;
	private String consumables;
	private String hyperdriveRating;
	private String mglt;
	private String starshipClass;
	private ArrayList<String> pilots;
	private ArrayList<String> films;
	private String created;
	private String edited;
	private String url;
	
	public Starship(@JsonProperty("name") String name, @JsonProperty("model") String model,
			@JsonProperty("manufacturer") String manufacturer, @JsonProperty("cost_in_credits") String cost,
			@JsonProperty("length") String length, @JsonProperty("max_atmosphering_speed") String maxAtmospheringSpeed,
			@JsonProperty("crew") String crew, @JsonProperty("passengers") String passengers,
			@JsonProperty("cargo_capacity") String cargoCapacity,	@JsonProperty("consumables") String consumables,
			@JsonProperty("hyperdrive_rating") String hyperdriveRating, @JsonProperty("MGLT") String mglt,
			@JsonProperty("starship_class") String starshipClass, @JsonProperty("pilots") ArrayList<String> pilots,
			@JsonProperty("films") ArrayList<String> films, @JsonProperty("created") String created,
			@JsonProperty("edited") String edited, @JsonProperty("url") String url) {
		this.setName(name);
		this.setModel(model);
		this.setManufacturer(manufacturer);
		this.setCost(cost);
		this.setLength(length);
		this.setMaxAtmospheringSpeed(maxAtmospheringSpeed);
		this.setCrew(crew);
		this.setPassengers(passengers);
		this.setCargoCapacity(cargoCapacity);
		this.setConsumables(consumables);
		this.setHyperdriveRating(hyperdriveRating);
		this.setMglt(mglt);
		this.setStarshipClass(starshipClass);
		this.setPilots(pilots);
		this.setFilms(films);
		this.setCreated(created);
		this.setEdited(edited);
		this.setUrl(url);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getMaxAtmospheringSpeed() {
		return maxAtmospheringSpeed;
	}

	public void setMaxAtmospheringSpeed(String maxAtmospheringSpeed) {
		this.maxAtmospheringSpeed = maxAtmospheringSpeed;
	}

	public String getCrew() {
		return crew;
	}
	
	public int getCrewNormalizedAsInt() {
		try {
			return Integer.parseInt(this.getCrew());
		}catch (Exception e) {
			return 0;
		}
	}

	public void setCrew(String crew) {
		this.crew = crew;
	}

	public String getPassengers() {
		return passengers;
	}
	
	public int getPassengersNormalizedAsInt() {
		try {
			return Integer.parseInt(this.getPassengers());
		}catch (Exception e) {
			return 0;
		}
	}

	public void setPassengers(String passengers) {
		this.passengers = passengers;
	}

	public String getCargoCapacity() {
		return cargoCapacity;
	}

	public void setCargoCapacity(String cargoCapacity) {
		this.cargoCapacity = cargoCapacity;
	}

	public String getConsumables() {
		return consumables;
	}

	public void setConsumables(String consumables) {
		this.consumables = consumables;
	}

	public String getHyperdriveRating() {
		return hyperdriveRating;
	}
	
	public int getHyperdriveRatingNormalizedAsInt() {
		try {
			return (int) (Double.parseDouble(this.getHyperdriveRating()) * 10);
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public void setHyperdriveRating(String hyperdriveRating) {
		this.hyperdriveRating = hyperdriveRating;
	}

	public String getMglt() {
		return mglt;
	}
	
	public int getMgltNormalizedAsInt() {
		try {
			return Integer.parseInt(this.getMglt());
		}catch (Exception e) {
			return 0;
		}
	}

	public void setMglt(String mglt) {
		this.mglt = mglt;
	}

	public String getStarshipClass() {
		return starshipClass;
	}

	public void setStarshipClass(String starshipClass) {
		this.starshipClass = starshipClass;
	}

	public ArrayList<String> getPilots() {
		return pilots;
	}

	public void setPilots(ArrayList<String> pilots) {
		this.pilots = pilots;
	}

	public ArrayList<String> getFilms() {
		return films;
	}

	public void setFilms(ArrayList<String> films) {
		this.films = films;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getEdited() {
		return edited;
	}

	public void setEdited(String edited) {
		this.edited = edited;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}